# BarbBlock Logo

Copyright (c) 2017 Paul Butler, MIT

Source: https://raw.githubusercontent.com/paulgb/BarbBlock/master/extension/icon/icon128.png

The PNG files included in this directory are distributed under the same license as the original work (see [LICENSE](./LICENSE)).
