# Energized Protection Logo

Copyright © 2018-2021 Energized Protection, MIT

Source: https://avatars.githubusercontent.com/u/39132478?s=200&v=4

The PNG files included in this directory are distributed under the same license as the original work (see [LICENSE](./LICENSE)).
