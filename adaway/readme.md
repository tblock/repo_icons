# AdAway Logo

AdAway, GPL-3.0 \<https://www.gnu.org/licenses/gpl-3.0>

Source: https://raw.githubusercontent.com/AdAway/adaway.github.io/master/assets/img/logo.svg

The PNG files included in this directory are distributed under the same license as the original work (see [LICENSE](./LICENSE)).
