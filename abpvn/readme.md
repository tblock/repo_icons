# abpvn Logo

ABPVN, GPL-3.0 \<https://www.gnu.org/licenses/gpl-3.0>

Source: https://abpvn.com/icon.png

The PNG files included in this directory are distributed under the same license as the original work (see [LICENSE](./LICENSE)).
