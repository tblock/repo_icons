# The Blocklist Project Logo

The Blocklist Project, Unlicense \<https://unlicense.org>

Source: https://raw.githubusercontent.com/blocklistproject/Lists/master/img/logo.webp

The PNG files included in this directory are distributed under the same license as the original work (see [LICENSE](./LICENSE)).
