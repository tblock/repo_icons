#!/bin/sh

mkdir -p __tmp_icons
for x in */*.png
	do cp $x __tmp_icons
done
cd __tmp_icons && tar -cvJf ../icons.tar.xz ./*.png && cd ..
rm -rf __tmp_icons

