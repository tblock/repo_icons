# Exodus Privacy Logo

Exodus Privacy, AGPL-3.0

Source: https://exodus-privacy.eu.org/media/logo/exodus.png

The PNG files included in this directory are distributed under the same license as the original work (see [LICENSE](./LICENSE)).
