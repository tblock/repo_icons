# AdGuard Logo

KenigNat, CC BY-SA 4.0 \<https://creativecommons.org/licenses/by-sa/4.0>, via Wikimedia Commons

Source: https://commons.wikimedia.org/wiki/File:AdGuard.svg

The PNG files included in this directory are distributed under the same license as the original work (see [LICENSE](./LICENSE)).
