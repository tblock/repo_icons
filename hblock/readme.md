# hBlock Logo

Copyright © 2022 Héctor Molinero Fernández, MIT

Source: https://raw.githubusercontent.com/hectorm/hblock/master/resources/logo/vectors/logo-shield.svg

The PNG files included in this directory are distributed under the same license as the original work (see [LICENSE](./LICENSE)).
