# Antifa Logo

M(a)nny, Creative Commons Attribution-Share Alike 3.0 Unported

Source: https://commons.wikimedia.org/wiki/File:Antifaschistische_Aktion_(mit_Schwarzer_und_Roter_Fahne).svg

The PNG files included in this directory are distributed under the same license as the original work (see [LICENSE](./LICENSE)).
